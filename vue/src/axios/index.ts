import axios from "axios";

let user = JSON.parse(localStorage.getItem("user") || "{ \"token\": \"\"}");

export default axios.create({
  baseURL: 'http://127.0.0.1:3000',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: "Bearer " + user.token
  }
});