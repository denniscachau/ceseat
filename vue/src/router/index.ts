import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Restaurant from '../views/Restaurant.vue'
import RestaurantForm from '../views/RestaurantForm.vue'
import AccountForm from '../views/AccountForm.vue'
import Menu from '../views/Menu.vue'
import History from '../views/History.vue'
import Article from '../views/Article.vue'
import Panier from '../views/Panier.vue'
import ArticlesForm from '../views/ArticlesForm.vue'
import MenuForm from '../views/MenuForm.vue'
import LegalNotices from '../views/LegalNotices.vue'
import PrivacyPolicy from '../views/PrivacyPolicy.vue'
import AllAccounts from '../views/AllAccounts.vue'
import Statistics from '../views/Statistics.vue'
import Developper from '../views/Developper.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/restaurant/:id_restaurant',
    name: 'Restaurant',
    component: Restaurant
  },
  {
    path: '/restaurant/add',
    name: 'RestaurantAdd',
    component: RestaurantForm
  },
  {
    path: '/restaurant/edit/:id_restaurant',
    name: 'RestaurantEdit',
    component: RestaurantForm
  },
  {
    path: '/restaurant/:id_restaurant/menu/:id_menu',
    name: 'Menu',
    component: Menu
  },
  {
    path: '/history',
    name: 'History',
    component: History
  },
  {
    path: '/restaurant/:id_restaurant/article/:id_article',
    name: 'Article',
    component: Article
  },
  {
    path: '/cart',
    name: 'Panier',
    component: Panier
  },
  {
    path: '/restaurant/:id_restaurant/article/add',
    name: 'ArticleAdd',
    component: ArticlesForm
  },
  {
    path: '/restaurant/:id_restaurant/article/edit/:id_article',
    name: 'ArticleEdit',
    component: ArticlesForm
  },
  {
    path: '/account',
    name: 'AccountForm',
    component: AccountForm
  },
  {
    path: '/restaurant/:id_restaurant/menu/add',
    name: 'MenuAdd',
    component: MenuForm
  },
  {
    path: '/restaurant/:id_restaurant/menu/edit/:id_menu',
    name: 'MenuEdit',
    component: MenuForm
  },
  {
    path: '/legal-notices',
    name: 'LegalNotices',
    component: LegalNotices
  },
  {
    path: '/privacy-policy',
    name: 'PrivacyPolicy',
    component: PrivacyPolicy
  },
  {
    path: '/all-accounts',
    name: 'AllAccounts',
    component: AllAccounts
  },
  {
    path: '/statistics',
    name: 'Statistics',
    component: Statistics
  },
  {
    path: '/developper',
    name: 'Developper',
    component: Developper
  },
  {
    path: '/:pathMatch(.*)*',
    redirect: '/home'
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router