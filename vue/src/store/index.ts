import { createStore } from 'vuex'
import axios from "@/axios/index";
import router from '@/router';

let storage: any = JSON.parse(localStorage.getItem('user') || "{}");
let cart: any = JSON.parse(localStorage.getItem('cart') || '[]');
let restaurant_info: any = JSON.parse(localStorage.getItem('restaurant_info') || '{}');

export default createStore({
  state: {
    isLogged: storage && storage.token ? true : false,
    user: storage && storage.token ? storage : null,
    cart: cart,
    restaurant_info: restaurant_info,
    alertMessage: null as null|string,
    error: false,
    success: false,
  },
  mutations: {
    loginSuccess(state, storage) {
      state.isLogged = true;
      state.user = storage;
      state.cart = [];
      axios.defaults.headers['Authorization'] = "Bearer " + storage.token
      localStorage.setItem('user', JSON.stringify(storage));
    },
    loginFailure(state) {
      state.isLogged = false;
      state.user = null;
      state.cart = [];
      localStorage.setItem('user', JSON.stringify("{}"));
    },
    logout(state) {
      state.isLogged = false;
      state.user = null;
      state.cart = [];
      axios.defaults.headers.common['Authorization'] = ""
      localStorage.setItem('user', JSON.stringify("{}"));
    },
    checkAccess(state: any, data: any) {
      if (data.mustBeLogged && !state.isLogged) {
        router.push({ name: "Login" })
      } else if (data.roles && data.roles.indexOf(state.user.user.role) == -1) {
        if (state.user.user.role == "commercial") {
          router.push({ name: "Statistics" })
        } else if (state.user.user.role == "deliver") {
          router.push({ name: "History" })
        } else if (state.user.user.role == "3rd_party_dev") {
          router.push({ name: "Developper" })
        } else {
          router.push({ name: "Home" })
        }
      }
    },
    addToCart(state: any, article: any) {
      let existing = false;
      if (!article.articles) {
        for (let a of state.cart) {
          if (a._id == article._id) {
            a.quantity += article.quantity;
            a.price += article.price;
            existing = true;
            break;
          }
        }
      }

      if (!existing) {
        state.cart.push(article);
      }

      localStorage.setItem('cart', JSON.stringify(state.cart));
    },
    deleteArticleFromCart(state, article_id) {
      
      let filtered = false;
      state.cart = state.cart.filter((article: any) => {
        if (article._id != article_id || filtered) {
          return true;
        } else {
          filtered = true
          return false;
        }
      });
      
      localStorage.setItem('cart', JSON.stringify(state.cart));

      if(state.cart.length == 0){
        state.restaurant_info = {}
        localStorage.setItem('restaurant_info', JSON.stringify(state.restaurant_info));
      }
      
    },
    
    deleteAllArticleFromCart(state) {
      state.cart = [];
      
      localStorage.setItem('cart', JSON.stringify(state.cart));
      if(state.cart.length == 0){
        state.restaurant_info = {}
        localStorage.setItem('restaurant_info', JSON.stringify(state.restaurant_info));
      }
      
    },

    saveRestaurantData(state, restaurant){
      
      state.restaurant_info.name = restaurant.name 
      state.restaurant_info.address = restaurant.address
      state.restaurant_info.id = restaurant._id
      state.restaurant_info.owner_info = restaurant.owner_info
      state.restaurant_info.image = restaurant.image
      
      localStorage.setItem('restaurant_info', JSON.stringify(state.restaurant_info));
    }
  },
  actions: {
    handleResponse(store, data) {
      let response = data.response.response;
      let access = data.access;

      store.commit('checkAccess', access)
      
      if (data.response.status >= 200 && data.response.status < 300) {
        store.state.error = false;
        store.state.alertMessage = null;
        return true
      } else if (response.data && response.data.name) {
        store.state.error = true;
        switch (response.data.name) {
          case 'RefreshTokenExpiredError':
          case 'TokenExpiredError':
            store.state.alertMessage = "Token expiré, veuillez vous reconnecter";
            this.commit('logout')
            return router.push({ name: "Login" });
          default:
            store.state.alertMessage = response.data.message;
            break;
        }
      }
    },
    sendNotifications(store, data) {
      console.log(data);
      let notification = new Notification(data.title, {
        body: data.message,
      });
    }
  }
})
