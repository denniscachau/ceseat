let express = require('express');
let logger = require('morgan');
let secure = require('./routes/secure');
let account = require('./routes/account')
let article = require('./routes/article')
let component = require('./routes/component')
let login = require('./routes/login')
let menu = require('./routes/menu')
let order = require('./routes/order')
let register = require('./routes/register')
let stats = require('./routes/stats')
let restaurant = require('./routes/restaurant')
let logout = require('./routes/logout')
let cors = require('cors');
let uploadImage = require('./routes/upload-image');

let app = express();

app.listen('3001');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors())


//pas de check jwt
app.use('/login', login);
app.use('/register', register);
app.use('/public', express.static('public'));

//check jwt
// app.use('*', secure);

app.use('/account', account);
app.use('/component', component);
app.use('/order', order);
app.use('/stats', stats);
app.use('/article', article);
app.use('/menu', menu);
app.use('/restaurant', restaurant);
app.use('/logout', logout);
app.use('/upload-image', uploadImage);
