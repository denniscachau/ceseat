let express = require('express');

let router = express.Router();

let accountModel = require('../models/user')

router.get('/', (req, res) => {

    accountModel.findAll({order: [
        ["id", "desc"]
    ]})
        .then((acc) => {
            res.json(acc)
        })
        .catch(
            (error) => {
                res.status(404).json({ message: "Aucun utilisateur" })
            })

});

router.get('/:id', (req, res) => {

    accountModel.findOne({ where: { id: req.params.id } })
        .then((acc) => {
            res.json(acc)
        })
        .catch(
            (error) => {
                res.status(404).json({ message: "L'utilisateur n'existe pas." })
            })

});

router.put('/:id', (req, res) => {

    let body = {

        firstname: req.body.firstname, 
        lastname: req.body.lastname, 
        email: req.body.email,
        address: req.body.address,
        postcode: req.body.postcode,
        city: req.body.city

    }


    accountModel.update(body, {where: {id: req.params.id}})
    .then((acc) => {
        res.json(acc)
    })
    .catch(
        (error) => {
            res.status(404).json({ message: "L'utilisateur n'a pas été modifié." })
        })

});

router.delete('/:id', (req, res) => {
    
    accountModel.destroy({where: {id: req.params.id}})
    .then((acc) => {
        res.json(acc)
    })
    .catch(
        (error) => {
            res.status(404).json({ message: "L'utilisateur n'a pas été supprimé." })
        })


});

router.put('/suspend/:id', (req, res) => {

    accountModel.update({active: req.body.active}, {where: {id: req.params.id}})
    .then((acc) => {
        res.json(acc)
    })
    .catch(
        (error) => {
            res.status(404).json({ message: "L'utilisateur n'a pas été suspendu." })
        })

});

router.put('/role/:id', (req, res) => {

    accountModel.update({role: req.body.role}, {where: {id: req.params.id}})
    .then((acc) => {
        res.json(acc)
    })
    .catch(
        (error) => {
            res.status(404).json({ message: "L'utilisateur n'a pas été modifié." })
        })

});

router.post('/sponsor', (req, res) => {
});

module.exports = router;
