let express = require('express');
let router = express.Router();

let userModel = require('../models/user')

router.get('/', (req, res) => {
    userModel.findAll()
        .then( (user) => {
            res.json(user)
        })
        .catch(
            (error) => {
                res.status(404).json(error)
            })
        
});

module.exports = router;
