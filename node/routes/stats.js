let express = require('express');

let router = express.Router();

let orderModel = require('../models/order');
let userModel = require('../models/user');

router.get('/orders-daily', (req, res) => {
  let date = new Date();
  let dateFormat = String(date.getFullYear()) + "-" + String(date.getMonth() + 1).padStart(2, '0');
  
  orderModel.aggregate([
    {
      "$match": {
        "$expr": {
          "$eq": [
            { "$dateToString": { "format": "%Y-%m", "date": "$created_at" } },
            dateFormat
          ]
        }
      }
    },
    {
      "$group": {
        "_id": { "$dateToString": { "format": "%Y-%m-%d", "date": "$created_at" } },
        "total": { "$sum": 1 }
      }
    }
  ]).then((orders) => {
    res.json(orders)
  })
});

router.get('/sales-monthly', (req, res) => {
  let date = new Date();
  let year = String(date.getFullYear());
  
  orderModel.aggregate([
    {
      "$match": {
        "$expr": {
          "$eq": [
            { "$dateToString": { "format": "%Y", "date": "$created_at" } },
            year
          ]
        }
      }
    },
    {
      "$group": {
        "_id": { "$dateToString": { "format": "%Y-%m", "date": "$created_at" } },
        "total": { "$sum": "$price" }
      }
    }
  ]).then((orders) => {
    res.json(orders)
  })
});

router.get('/registers', (req, res) => {
  userModel.count({group: ['role']})
    .then(people => res.status(200).json(people))
    .catch(error => res.status(400).json({ message: "Aucun utilisateur n'a été trouvé." }));
});

router.get('/total-orders', (req, res) => {
  orderModel.aggregate([
    {
      "$group": {
        "_id": "$restaurant_info.name",
        "total": { "$sum": 1 }
      }
    }
  ]).then((orders) => {
    res.json(orders)
  })
});

module.exports = router;
