let express = require('express');
let mongoose = require('mongoose')

let router = express.Router();

let articleModel = require('../models/article')
let restaurantModel = require('../models/restaurant')

router.post('/', (req, res) => {
    let restaurant_id = mongoose.Types.ObjectId(req.body.restaurant_id);

    restaurantModel.findOne({ "_id": restaurant_id })
        .then(restaurant_info => {

            let restaurant_temp = JSON.parse(JSON.stringify(restaurant_info))
            restaurant_temp.id = req.body.restaurant_id
            restaurant_temp._id = mongoose.Types.ObjectId(req.body.restaurant_id);

            let body = {
                restaurant_info: restaurant_temp,
                name: req.body.name,
                type: req.body.type,
                price: req.body.price,
                image: req.body.image
            }
            
            return articleModel.create(body)
        })
        .then(article => res.status(200).json(article))
        .catch(error => res.status(400).json({ message: "L'article n'a pas pu être créé." }));

});

router.get('/restaurant/:id_restaurant', (req, res) => {

    articleModel.find({"restaurant_info._id" : mongoose.Types.ObjectId(req.params.id_restaurant)})
        .then(article => {
            res.status(200).json(article)
        })
        .catch(error => res.status(400).json({ message: "Aucun article dans ce restaurant n'a été trouvé." }));

});

router.get('/types/:id_restaurant', (req, res) => {

    articleModel.aggregate([
        { "$match": { "restaurant_info._id": mongoose.Types.ObjectId(req.params.id_restaurant) } },
        {
            "$group": {
                "_id": "$type"
            }
        }
    ]).then(types => {
        for (let type of types) {
            type.name = type._id;
        }
        res.json(types)
    }).catch(error => res.status(400).json(error));
})

router.get('/', (req, res) => {

    articleModel.find({})
        .then(article => res.status(200).json(article))
        .catch(error => res.status(400).json({ message: "Aucun article n'a été trouvé." }));

});

router.get('/:id', (req, res) => {

    articleModel.findOne({ _id: req.params.id })
        .then(article => res.status(200).json(article))
        .catch(error => res.status(400).json({ message: "L'article n'existe pas." }));

});

router.put('/:id', (req, res) => {

    let body = {
        name: req.body.name,
        type: req.body.type,
        price: req.body.price
    }

    if (req.body.image) {
        body.image = req.body.image
    };

    articleModel.findOneAndUpdate({ _id: req.params.id }, body)
        .then(article => res.status(200).json(article))
        .catch(error => res.status(400).json({ message: "L'article n'a pas été modifié." }));

});

router.delete('/:id', (req, res) => {

    articleModel.findOneAndDelete({ _id: req.params.id })
        .then(article => res.status(200).json(article))
        .catch(error => res.status(400).json({ message: "L'article n'a pas été supprimé." }));

});

module.exports = router;
