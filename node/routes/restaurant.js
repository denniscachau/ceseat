let express = require('express');

let router = express.Router();

let restaurantModel = require('../models/restaurant')

router.post('/', (req, res) => {

    let body = {
        owner_info: req.body.owner_info,
        name: req.body.name,
        description: req.body.description,
        address: req.body.address,
        postcode: req.body.postcode,
        city: req.body.city,
        created_at: req.body.created_at,
        image: req.body.image
    }


    restaurantModel.create(body)
        .then((acc) => {
            res.json(acc)
        })
        .catch(
            (error) => {
                res.status(404).json({ message: "Le restaurant n'a pas été crée." })
            })

});

router.get('/', (req, res) => {

    restaurantModel.find({})
        .then(restaurant => res.status(200).json(restaurant))
        .catch(error => res.status(400).json({ message: "Le restaurant n'a pas été supprimé." }));

});

router.get('/restaurateur/:id_restaurateur', (req, res) => {

    restaurantModel.find({ "owner_info.id_user": Number(req.params.id_restaurateur) })
        .then(restaurants => res.status(200).json(restaurants))
        .catch(error => res.status(404).json({ message: "Aucun restaurant n'a été trouvé." }));

});


router.get('/:id', (req, res) => {

    restaurantModel.findOne({ _id: req.params.id })
        .then(restaurant => res.status(200).json(restaurant))
        .catch(error => res.status(400).json({ message: "Le restaurant n'a pas été trouvé." }));

});

router.put('/:id', (req, res) => {

    let body = {
        owner_info: req.body.owner_info,
        name: req.body.name,
        description: req.body.description,
        address: req.body.address,
        postcode: req.body.postcode,
        city: req.body.city,
        image: req.body.image
    }

    restaurantModel.findOneAndUpdate({ _id: req.params.id }, body)
        .then(restaurant => res.status(200).json(restaurant))
        .catch(error => res.status(400).json({ message: "Le restaurant n'a pas été modifié." }));

});

router.delete('/:id', (req, res) => {

    restaurantModel.findOneAndDelete({ _id: req.params.id })
        .then(restaurant => res.status(200).json(restaurant))
        .catch(error => res.status(400).json({ message: "Le restaurant n'a pas été supprimé." }));

});



module.exports = router;
