let express = require('express');
let mongoose = require('mongoose');

let router = express.Router();

let menuModel = require('../models/menu')
let articleModel = require('../models/article');
let restaurantModel = require('../models/restaurant')

router.post('/', (req, res) => {
    let restaurant_id = mongoose.Types.ObjectId(req.body.restaurant_id);

    restaurantModel.findOne({ _id: restaurant_id })
        .then(restaurant_info => {

            let where = []
            for (let article of req.body.articles) {
                where.push({ _id: mongoose.Types.ObjectId(article) });
            }

            articleModel.find(
                {$or: where}
            ).then(articles => {

                let body_articles = {};
                for (let article of articles) {
                    if (!body_articles[article.type]) {
                        body_articles[article.type] = [];
                    }

                    body_articles[article.type].push(article.name)
                }
                let restaurant_temp = JSON.parse(JSON.stringify(restaurant_info))
                restaurant_temp.id = req.body.restaurant_id
                restaurant_temp._id = mongoose.Types.ObjectId(req.body.restaurant_id);

                let body = {

                    restaurant_info: restaurant_temp,
                    name: req.body.name,
                    description: req.body.description,
                    price: req.body.price,
                    articles: body_articles,
                    image: req.body.image

                }
                
                menuModel.create(body)
                    .then(menu => res.status(200).json(menu))
            })
        })
        .catch(() => res.status(400).json({ message: "Le menu n'a pas été créé." }));

});

router.get('/', (req, res) => {

    menuModel.find({})
        .then(menu => res.status(200).json(menu))
        .catch(error => res.status(400).json({ message: "Aucun menu n'a été trouvé." }));

});

router.get('/restaurant/:id_restaurant', (req, res) => {

    menuModel.find({"restaurant_info._id" : mongoose.Types.ObjectId(req.params.id_restaurant) })
        .then(menu => res.status(200).json(menu))
        .catch(error => res.status(400).json({ message: "Aucun menu n'a été trouvé." }));

});

router.get('/:id', (req, res) => {

    menuModel.findOne({ _id: req.params.id })
        .then(menu => res.status(200).json(menu))
        .catch(error => res.status(400).json({ message: "Le menu n'a pas été trouvé." }));

});

router.put('/:id', (req, res) => {

    let restaurant_id = mongoose.Types.ObjectId(req.body.restaurant_id);

    restaurantModel.findOne({ _id: restaurant_id })
        .then(restaurant_info => {

            let where = []
            for (let article of req.body.articles) {
                where.push({ _id: mongoose.Types.ObjectId(article) });
            }

            articleModel.find(
                {$or: where}
            ).then(articles => {
                console.log("Articles:", articles);

                let body_articles = {};
                for (let article of articles) {
                    if (!body_articles[article.type]) {
                        body_articles[article.type] = [];
                    }

                    body_articles[article.type].push(article.name)
                }

                let body = {

                    restaurant_info: restaurant_info,
                    name: req.body.name,
                    description: req.body.description,
                    price: req.body.price,
                    articles: body_articles,
                    image: req.body.image

                }

                console.log("BODYYY", body);
                
                return menuModel.findOneAndUpdate({ _id: req.params.id }, body)
            })
        })
        
        .then(menu => res.status(200).json(menu))
        .catch(() => res.status(400).json({ message: "Le menu n'a pas été modifié." }));

});

router.delete('/:id', (req, res) => {

    menuModel.findOneAndDelete({ _id: req.params.id })
        .then(menu => res.status(200).json(menu))
        .catch(error => res.status(400).json({ message: "Le menu n'a pas été supprimé." }));

});

module.exports = router;
