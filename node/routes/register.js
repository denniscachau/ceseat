let express = require('express');

let router = express.Router();

let accountModel = require('../models/user')

router.post('/', (req, res) => {

    let body = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: req.body.password,
        sponsored_by: req.body.sponsored_by,
        id_sponsor: req.body.id_sponsor,
        active: 1
    }


    accountModel.create(body)
        .then((acc) => {
            res.json(acc)
        })
        .catch(
            (error) => {
                res.status(404).json(error)
            })

});

module.exports = router;