let express = require('express');

let router = express.Router();

let orderModel = require('../models/order')

router.post('/', (req, res) => {
    let body = {
        client_info: req.body.client_info,
        restaurant_info: req.body.restaurant_info,
        status: req.body.status,
        price: req.body.price,
        details: req.body.details,
        is_validated: req.body.is_validated,
        delivers_info: req.body.delivers_info,
        created_at: req.body.created_at,
        status_key: req.body.status_key
    }

    orderModel.create(body)
        .then(order => res.status(200).json(order))
        .catch(error => res.status(400).json({ message: "La commande n'a pas été créée." }));

});

//récup toute les commandes pour le livreur
router.get('/history/deliver', (req, res) => {

    orderModel.find({$or: [{status_key: "validated"}, {status_key: "cancelByDeliver"}, {status_key: "took"}] }).sort([['created_at', -1]])
        .then(order => res.status(200).json(order))
        .catch(error => res.status(400).json({ message: "Aucune commande n'existe." }));

});

//récup toute les commandes pour le restaurateur
router.get('/history/restaurateur/:id', (req, res) => {

    orderModel.find({ "restaurant_info.owner_info.id_user": Number(req.params.id) }).sort([['created_at', -1]])
        .then(order => res.status(200).json(order))
        .catch(error => res.status(400).json({ message: "Aucune commande n'existe." }));

});

//récup toute les commandes pour l'utilisateur'
router.get('/history/user/:id', (req, res) => {

    orderModel.find({ "client_info.id_user": Number(req.params.id) }).sort([['created_at', -1]])
        .then(order => res.status(200).json(order))
        .catch(error => res.status(400).json({ message: "Aucune commande n'existe." }));

});

router.get('/:id', (req, res) => {

    orderModel.findOne({ _id: req.params.id })
        .then(order => res.status(200).json(order))
        .catch(error => res.status(400).json({ message: "La commande n'a pas été trouvée." }));

});

router.put('/:id', (req, res) => {

    let body = {
        client_info: req.body.client_info,
        restaurant_info: req.body.restaurant_info,
        price: req.body.price,
        details: req.body.details

    }

    orderModel.findOneAndUpdate({ _id: req.params.id }, body)
        .then(order => res.status(200).json(order))
        .catch(error => res.status(400).json({ message: "La commande n'a pas été modifiée." }));

});

router.delete('/:id', (req, res) => {
    orderModel.findOneAndDelete({ _id: req.params.id })
        .then(order => res.status(200).json(order))
        .catch(error => res.status(400).json({ message: "La commande n'a pas été supprimée." }));
});

router.put('/status/:id', (req, res) => {

    let body = {
        status: req.body.status,
        status_key: req.body.status_key
    }

    orderModel.findOneAndUpdate({ _id: req.params.id }, body)
        .then(order => res.status(200).json(order))
        .catch(error => res.status(400).json({ message: "Le statut de la commande n'a pas été modifié." }));
    
});

module.exports = router;
