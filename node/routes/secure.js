let express = require('express');
let jwt = require('../modules/jwt');
let userModel = require('../models/user');
let crypto = require('crypto');
let logger = require('../modules/logger');
const { ExceptionHandler } = require('winston');

let router = express.Router();

router.use('*', (req, res, next) => {
    if (!req.headers.authorization) {
        return res.send('') //.json({ message: "Il manque l'authorization dans le header" });
    }
    let bearer = req.headers.authorization.replace('Bearer ', '');

    try {
        jwt.verify(bearer);
        next();
    } catch (e) {
        if (e.name == "TokenExpiredError") {
            userModel.findOne({ where: { jwt: bearer } })
                .then(user => {
                    if (typeof user !== "undefined" && user !== null && user.expiresAt > Math.floor(Date.now() / 1000)) {
                        let token = jwt.create({ name: user.name, role: user.role });
                        let refreshToken = crypto.randomBytes(128).toString("base64");

                        userModel.update(
                            { jwt: token, refreshToken: refreshToken },
                            { where: { refreshToken: user.refreshToken } }
                        )

                        console.log('Nouveau jwt créé !')
                        next();
                    } else {
                        throw e.message
                    }
                }).then(() => {
                    console.log('Nouveau jwt créé !')
                    next();
                }).catch(error => {
                    logger.info("Token expiré");
                    res.status(400).json({ error: "Token expiré, veuillez vous reconnecter", name: "RefreshTokenExpiredError" })
                });
        } else {
            logger.info(e.message);
            res.status(400).json({ e })
        }
    }
});

module.exports = router;
