let express = require('express');
let jwt = require('../modules/jwt')
let crypto = require('crypto');
let router = express.Router();

let accountModel = require('../models/user')

router.put('/', (req, res) => {

    accountModel.update({refreshToken: "", jwt: "", expiresAt: Date.now()},{ where: {id: req.body.id} })
        .then((acc) => {
            res.json(acc);
        })
        .catch(
            (error) => {
                res.status(404).json({ message: "L'utilisateur est inconnu."})
            })

});

module.exports = router;
