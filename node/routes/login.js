let express = require('express');
let jwt = require('../modules/jwt')
let router = express.Router();

let accountModel = require('../models/user')

router.post('/', (req, res) => {

    accountModel.findOne({ where: { email: req.body.email, password: req.body.password } })
        .then((acc) => {
            
            if(acc.active == 0){
                res.status(403).json({ message: "Votre compte a été suspendu" })
            }
            let user = {
                id_user: acc.id,
                firstname: acc.firstname,
                lastname: acc.lastname,
                role: acc.role,
                email: acc.email,
                sponsored_by: acc.sponsored_by,
            }
            let token = jwt.create(user);

            accountModel.update({
                jwt: token
            }, {
                where: { id: acc.id }
            }).then(() => {
                return res.json({
                    token: token,
                    user: user
                })
            })
        })
        .catch(
            (error) => {
                res.status(404).json({ message: "L'email ou le mot de passe est erroné." })
            })

});

module.exports = router;