let db = require('./nosqlDb');

const orderSchema = new db.Schema({
    client_info: { type: Object, required: true },
    restaurant_info: { type: Object, required: true },
    status: { type: String, required: true },
    price: { type: Number, required: true},
    details: { type: Array, required: false},
    status_key: { type: String, required: true},
    delivers_info: { type: Array, required: false},
    created_at: { type: Date, required: true},
});

const orderModel = db.model('orders', orderSchema);
module.exports = orderModel;
