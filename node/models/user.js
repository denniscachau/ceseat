let db = require('./mysqlDb');
let Sequelize = require('sequelize')
    /*
    const userSchema = new db.Schema({
        fullname: { type: String, required: false },
        lastname: { type: String, required: false },
        email: { type: String, requires: false },
        password: { type: String, required: false},
        address: { type: String, required: false},
        postcode: { type: Number, required: false},
        city: { type: String, required: false},
        birth_date: { type: Date, required: false},
        active: { type: String, required: false},
        sponsored_by: { type: String, required: false},
        id_sponsor: { type: Number, required: false},
        created_at: { type: Datetime, required: false},
        updated_at: { type: Datetime, required: false},
    });

    const userModel = db.model('d_users', userSchema);
    module.exports = userModel;
    */

module.exports = db.define("users", {
    id: {
        type: Sequelize.INTEGER(11),
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    firstname: {
        type: Sequelize.STRING(255),
        allowNull: false
    },
    lastname: {
        type: Sequelize.STRING(255),
        allowNull: false
    },
    email: {
        type: Sequelize.STRING(255),
        allowNull: false
    },
    password: {
        type: Sequelize.STRING(255),
        allowNull: false
    },
    address: {
        type: Sequelize.STRING(255),
        allowNull: true
    },
    postcode: {
        type: Sequelize.INTEGER(5),
        allowNull: true
    },
    city: {
        type: Sequelize.STRING(255),
        allowNull: true
    },
    birth_date: {
        type: Sequelize.DATE,
        allowNull: true
    },
    active: {
        type: Sequelize.TINYINT(1),
        allowNull: false,
        defaultValue: 1
    },
    sponsored_by: {
        type: Sequelize.STRING(255),
        allowNull: true
    },
    id_sponsor: {
        type: Sequelize.INTEGER(5),
        allowNull: false,
    },
    role: {
        type: Sequelize.ENUM('user', 'restaurateur', 'deliver', '3rd_party_dev'),
        defaultValue: 'user'
    },
    refreshToken: {
        type: Sequelize.STRING(255),
        allowNull: true
    },
    expiresAt: {
        type: Sequelize.DATE,
        allowNull: true
    },
    jwt: {
        type: Sequelize.STRING(255),
        allowNull: true
    },
});