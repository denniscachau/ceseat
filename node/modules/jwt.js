let jwt = require('jsonwebtoken');

function create(user) {
    return jwt.sign(user, 'Soube', {expiresIn: "10h"});
}

function verify(token) {
    return jwt.verify(token, 'Soube');
}

module.exports = {'create': create, 'verify': verify};
