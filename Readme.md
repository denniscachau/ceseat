Pour ce projet, nous devions re-créer les fonctionnalités de base de l'application de bureau Uber'Eat en 10 jours. Nous étions un groupe de 4 personnes. 

Cette application a été développée en Vue.js pour le frontend et Node.js (framework express) pour le backend.
Nous avons utilisé MongoDb pour la base de données noSQL.

Le déploiement s'est fait à l'aide de Docker et de Kubernetes.