let express = require('express');
let fetch = require('node-fetch')

let router = express.Router();
let crypto = require('crypto');

router.post('/', (req, res) => {

    if (req.body.firstname && req.body.lastname && req.body.email && req.body.password) {
        let body = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: req.body.password,
            sponsored_by: req.body.sponsored_by,
            id_sponsor: crypto.randomBytes(64).toString("base64"),
            active: 1
        }

        fetch(`${process.env.FETCH_URL}/register`, {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw { message: "Un compte existe déjà avec cette adresse email." }
                }
            })
            .catch(
                (error) => {
                    res.status(400).json({ message: error.message })
                })
    } else {
        res.status(401).json({ message: "Veuillez renseigner les informations demandées." })
    }

});

module.exports = router;