let express = require('express');
let fetch = require('node-fetch');

let router = express.Router();
let jwt = require('../modules/jwt');

router.post('/', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        let body = {
            owner_info: req.body.owner,
            name: req.body.name,
            description: req.body.description,
            address: req.body.address,
            postcode: req.body.postcode,
            city: req.body.city,
            created_at: Date.now(),
            image: req.body.image
        }

        fetch(`${process.env.FETCH_URL}/restaurant/`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Le restaurant n'a pas pu être enregistré, veuillez vérifier que toutes les informations ont bien été rentrées."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous devez être connecté en tant que restaurateur pour pouvoir créer un restaurant." })
    }
});

router.get('/', (req, res) => {
    fetch(`${process.env.FETCH_URL}/restaurant/`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Il n'y a aucun restaurant de disponible."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.get('/restaurateur/:id_restaurateur', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur" && user.id_user == req.params.id_restaurateur) {
        fetch(`${process.env.FETCH_URL}/restaurant/restaurateur/${req.params.id_restaurateur}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Aucun restaurant."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous devez être connecté en tant que restaurateur pour pouvoir afficher vos restaurants." })
    }

});


router.get('/:id', (req, res) => {
    fetch(`${process.env.FETCH_URL}/restaurant/${req.params.id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Ce restaurant n'existe pas."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.put('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        let body = {
            owner_info: req.body.owner,
            name: req.body.name,
            description: req.body.description,
            address: req.body.address,
            postcode: req.body.postcode,
            city: req.body.city,
            image: req.body.image
        }

        fetch(`${process.env.FETCH_URL}/restaurant/${req.params.id}`, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Le restaurant n'a pas pu être enregistré, veuillez vérifier que toutes les informations ont bien été rentrées."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous devez être connecté en tant que restaurateur pour pouvoir modifier le restaurant." })
    }

});

router.delete('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        fetch(`${process.env.FETCH_URL}/restaurant/${req.params.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw { message: "Ce restaurant n'existe pas." }
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
    } else {
        res.status(403).json({ message: "Vous devez être connecté en tant que restaurateur pour pouvoir supprimer ce restaurant." })
    }
});

module.exports = router;
