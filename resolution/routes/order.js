let express = require('express');
let fetch = require('node-fetch');

let router = express.Router();
let jwt = require('../modules/jwt');

router.post('/', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "user") {
        let body = {
            client_info: req.body.client,
            restaurant_info: req.body.restaurant,
            status: "Commande envoyée au restaurant",
            price: req.body.price,
            details: req.body.details,
            is_validated: false,
            delivers_info: req.body.delivers,
            created_at: Date.now(),
            status_key: "wait"
        }

        fetch(`${process.env.FETCH_URL}/order/`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "La commande ne s'est pas enregistrée, veuillez vérifier que toues les informations ont bien été rentrées."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous devez être connecté en tant que client pour pouvoir passer une commande." })
    }
});

//récup toute les commandes pour le livreur
router.get('/history/deliver', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "deliver") {
        fetch(`${process.env.FETCH_URL}/order/history/deliver`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Aucune commande disponible."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour accéder à cette page." })
    }
});

//récup toute les commandes pour le restaurateur
router.get('/history/restaurateur/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        fetch(`${process.env.FETCH_URL}/order/history/restaurateur/${req.params.id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Aucune commande disponible."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour accéder à cette page." })
    }
});

//récup toute les commandes pour l'utilisateur'
router.get('/history/user/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "user") {
        fetch(`${process.env.FETCH_URL}/order/history/user/${req.params.id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Aucune commande disponible."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour accéder à cette page." })
    }
});

router.get('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "user") {
        fetch(`${process.env.FETCH_URL}/order/${req.params.id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Aucune commande disponible."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour accéder à cette page." })
    }
});

router.put('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "user") {
        let body = {
            price: req.body.price,
            details: req.body.details
        }

        fetch(`${process.env.FETCH_URL}/order/${req.params.id}`, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Aucune commande disponible."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour modifier cette commande." })
    }
});

router.delete('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "user") {
        fetch(`${process.env.FETCH_URL}/order/${req.params.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "La commande n'a pas pu être supprimé."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour supprimer cette commande." })
    }
});

router.put('/status/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "deliver" || user.role == "restaurateur") {
        let body = {
            status: req.body.status,
            status_key: req.body.status_key
        }

        fetch(`${process.env.FETCH_URL}/order/status/${req.params.id}`, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "La commande n'a pas pu être modifiée."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour supprimer cette commande." })
    }
});

module.exports = router;
