let express = require('express');
let jwt = require('../modules/jwt')
let router = express.Router();
let fetch = require('node-fetch');

router.put('/', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (req.body.id && user.id_user == req.body.id) {
        fetch(`${process.env.FETCH_URL}/logout`, {
            method: 'PUT',
            body: JSON.stringify({
                id: req.body.id,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            res.json(body)
        })
            .catch(() => {
            res.status(401).json({ message: "Veuillez vous connecter avant de vouloir vous déconnecter." })
        })
    } else {
        res.status(403).json({ message: "Veuillez vous connecter avant de vouloir vous déconnecter." })
    }
});

module.exports = router;
