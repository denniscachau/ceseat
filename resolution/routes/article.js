let express = require('express');
let fetch = require('node-fetch');
let jwt = require('../modules/jwt');

let router = express.Router();

router.post('/', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        let body = {
            restaurant_id: req.body.restaurant._id,
            name: req.body.name,
            type: req.body.type,
            price: req.body.price,
            image: req.body.image
        }

        fetch(`${process.env.FETCH_URL}/article/`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Veuilez vous assurer à ce que tous les champs soient bien remplis."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits pour ajouter cette article." })
    }
});

router.get('/restaurant/:id_restaurant', (req, res) => {
    fetch(`${process.env.FETCH_URL}/article/restaurant/${req.params.id_restaurant}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Aucun article dans ce restaurant n'a été trouvé."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.get('/types/:id_restaurant', (req, res) => {
    fetch(`${process.env.FETCH_URL}/article/types/${req.params.id_restaurant}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Aucun type n'est disponible pour cet article."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
})

router.get('/', (req, res) => {
    fetch(`${process.env.FETCH_URL}/article`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Aucun artcile n'a été trouvé."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.get('/:id', (req, res) => {
    fetch(`${process.env.FETCH_URL}/article/${req.params.id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "L'article n'a pas été trouvé."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.put('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        let body = {
            name: req.body.name,
            type: req.body.type,
            price: req.body.price
        }

        if (req.body.image) {
            body.image = req.body.image
        };
        
        fetch(`${process.env.FETCH_URL}/article/${req.params.id}`, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "L'article n'a pas été modifié."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    }
});

router.delete('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        fetch(`${process.env.FETCH_URL}/article/${req.params.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "L'article n'a pas été supprimé."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    }
});

module.exports = router;
