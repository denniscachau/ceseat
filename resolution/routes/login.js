let express = require('express');
let router = express.Router();
let fetch = require('node-fetch')

router.post('/', (req, res) => {
    if (req.body.email && req.body.password) {
        fetch(`${process.env.FETCH_URL}/login`, {
            method: 'POST',
            body: JSON.stringify({
                email: req.body.email,
                password: req.body.password,
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else if (response.status == 403) {
                res.status(401).json({ message: "Votre compte a été suspendu." })
            } else {
                throw response.json()
            }
        }).then(body => {
            res.json(body)
        })
            .catch((error) => {
                res.status(401).json({ message: "Votre email ou mot de passe est erroné." })
        })
    } else {
        res.status(401).json({ message: "Veuillez renseigner votre email et votre mot de passe." })
    }
});

module.exports = router;
