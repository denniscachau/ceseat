let express = require('express');
let jwt = require('../modules/jwt');

let router = express.Router();

router.use('*', (req, res, next) => {
    if (!req.headers.authorization) {
        return res.json({ message: "Il manque l'authorization dans le header" });
    }
    let bearer = req.headers.authorization.replace('Bearer ', '');

    try {
        jwt.verify(bearer);
        next();
    } catch (e) {
        res.status(401).json(e)
    }
});

module.exports = router;
