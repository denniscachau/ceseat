let express = require('express');

let router = express.Router();

router.post('/', (req, res) => {
});

router.get('/:id', (req, res) => {
});

router.delete('/:id', (req, res) => {
});

router.get('/download/:id', (req, res) => {
});

module.exports = router;
