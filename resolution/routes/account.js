let express = require('express');
let fetch = require('node-fetch');

let router = express.Router();
let jwt = require('../modules/jwt')

router.get('/', (req, res) => {

    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "commercial") {
        fetch(`${process.env.FETCH_URL}/account`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            console.log(response.status)
            if (response.status >= 200 && response.status < 300) {
                console.log(response)
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body2 => {
            
            if (body2 && !body2.errors) {
                res.json(body2)
            } else {
                throw { message: "Les utilisateurs n'existent pas." }
            }
        }).catch((error) => {
            console.log(error)
            res.status(401).json({ message: error.message })
        })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour modifier les utilisateurs" })
    }

});

router.get('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (req.params.id == user.id_user || user.role == "commercial") {
        fetch(`${process.env.FETCH_URL}/account/${req.params.id}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw { message: "L'utilisateur n'existe pas." }
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour modifier les utilisateurs" })
    }
});

router.put('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (req.params.id == user.id_user || user.role == "commercial") {
        let body = {
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            address: req.body.address,
            postcode: req.body.postcode,
            city: req.body.city
        }

        fetch(`${process.env.FETCH_URL}/account/${req.params.id}`, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw { message: "L'utilisateur n'existe pas." }
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour modifier les utilisateurs" })
    }
});

router.delete('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (req.params.id == user.id_user || user.role == "commercial") {
        fetch(`${process.env.FETCH_URL}/account/${req.params.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw { message: "L'utilisateur n'existe pas." }
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour modifier les utilisateurs" })
    }
});

router.put('/suspend/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "commercial") {
        fetch(`${process.env.FETCH_URL}/account/suspend/${req.params.id}`, {
            method: 'PUT',
            body: JSON.stringify({
                active: req.body.active
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw { message: "L'utilisateur n'existe pas." }
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour modifier les utilisateurs" })
    }
});

router.put('/role/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "commercial") {
        fetch(`${process.env.FETCH_URL}/account/role/${req.params.id}`, {
            method: 'PUT',
            body: JSON.stringify({
                role: req.body.role
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw { message: "L'utilisateur n'existe pas." }
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits d'accès pour modifier les utilisateurs" })
    }
});

router.post('/sponsor', (req, res) => {
});

module.exports = router;
