let express = require('express');
let jwt = require('../modules/jwt');
let userModel = require('../models/user');
let crypto = require('crypto');

let router = express.Router();

router.get('/create', (req, res) => {
    let token = jwt.create({name : req.body.name, role : req.body.role });

    let refreshToken = crypto.randomBytes(128).toString("base64");

    const userCreate = new userModel({
        name: req.body.name,
        role: req.body.role,
        expiresAt: Math.floor(Date.now()/1000) + 60*60*24*7,
        refreshToken: refreshToken,
        jwt: token
    });

    userCreate.save()
        .then(() => res.status(201).json({ message: 'Objet enregistré !' }))
        .catch(error => res.status(400).json({ error }));
});

router.get('/verify', (req, res) => {
    try {
        res.json(jwt.verify(req.query.token));
    } catch (e) {
        if (e.name == "TokenExpiredError") {
            userModel.findOne({ jwt: req.query.token })
            .then(user => {
                if (typeof user !== "undefined" && user.expiresAt > Math.floor(Date.now()/1000)) {
                    let token = jwt.create({name : user.name, role : user.role });
                    let refreshToken = crypto.randomBytes(128).toString("base64");

                    userModel.findOneAndUpdate(
                        {refreshToken: user.refreshToken},
                        {jwt: token, refreshToken: refreshToken}
                    ).then(() => res.status(200).json({ message: 'Nouveeau jwt créé !' }))
                    .catch(error => res.status(400).json({ error }))
                } else {
                    res.status(400).json({message: e.message});
                }
            });
        } else {
            console.error('out', e.name);
            res.status(400).json({message: e.message});
        }
    }
});

module.exports = router;
