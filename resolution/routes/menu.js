let express = require('express');
let fetch = require('node-fetch');
let jwt = require('../modules/jwt');

let router = express.Router();

router.post('/', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        let body = {
            restaurant_id: req.body.restaurant._id,
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            image: req.body.image,
            articles: req.body.articles
        }

        fetch(`${process.env.FETCH_URL}/menu/`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Veuilez vous assurer à ce que tous les champs soient bien remplis."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits pour ajouter ce menu." })
    }

});

router.get('/', (req, res) => {
    fetch(`${process.env.FETCH_URL}/menu/`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Aucun menu n'est disponible."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.get('/restaurant/:id_restaurant', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    fetch(`${process.env.FETCH_URL}/menu/restaurant/${req.params.id_restaurant}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Aucun menu n'a été trouvé."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.get('/:id', (req, res) => {
    fetch(`${process.env.FETCH_URL}/menu/${req.params.id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Le menu n'existe pas."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })

});

router.put('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        let body = {
            restaurant_id: req.body.restaurant._id,
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            image: req.body.image,
            articles: req.body.articles
        }

        fetch(`${process.env.FETCH_URL}/menu/${req.params.id}`, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Veuilez vous assurer à ce que tous les champs soient bien remplis."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits pour modifier ce menu." })
    }

});

router.delete('/:id', (req, res) => {
    let bearer = req.headers.authorization.replace('Bearer ', '');
    let user = jwt.verify(bearer);

    if (user.role == "restaurateur") {
        fetch(`${process.env.FETCH_URL}/menu/${req.params.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
            }).then(response => {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    throw response.json()
                }
            }).then(body => {
                if (body && !body.errors) {
                    res.json(body)
                } else {
                    throw {message: "Veuilez vous assurer à ce que tous les champs soient bien remplis."}
                }
            }).catch((error) => {
                res.status(401).json({ message: error.message })
            })
    } else {
        res.status(403).json({ message: "Vous n'avez pas les droits pour supprimer ce menu." })
    }

});

module.exports = router;
