let express = require('express');
let fetch = require('node-fetch');

let router = express.Router();

router.get('/orders-daily', (req, res) => {
  fetch(`${process.env.FETCH_URL}/stats/orders-daily/`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Stats indisponible."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.get('/sales-monthly', (req, res) => {
  fetch(`${process.env.FETCH_URL}/stats/sales-monthly/`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Stats indisponible."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.get('/registers', (req, res) => {
  fetch(`${process.env.FETCH_URL}/stats/registers/`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Stats indisponible."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

router.get('/total-orders', (req, res) => {
  fetch(`${process.env.FETCH_URL}/stats/total-orders/`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
        }).then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else {
                throw response.json()
            }
        }).then(body => {
            if (body && !body.errors) {
                res.json(body)
            } else {
                throw {message: "Stats indisponible."}
            }
        }).catch((error) => {
            res.status(401).json({ message: error.message })
        })
});

module.exports = router;
