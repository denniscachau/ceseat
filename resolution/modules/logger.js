const {createLogger, format, transports} = require('winston');

const logger = createLogger({
    level: 'info',
    exitOnError: false,
    format: format.prettyPrint(),
    transports: [new transports.File({filename: 'logs/badToken.log'})]
});

module.exports = logger;
